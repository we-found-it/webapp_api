const Tools = require('../app/utils/Tools');
const _ = require("lodash");
test('Get usernames from fullname', () => {

    const fullname = _.trim('Arthur LERAY').toLowerCase();
    const expectedUsernames = ['arthur-leray', 'arthur_leray'];

    expect(Tools.getUsernames(fullname)).toStrictEqual(expectedUsernames);
});