#!/bin/sh

# Remise en place des modules node afin qu'ils soient accessible par le développeur
rm -rf /app/node_modules
tar -xf /dependancies/node_modules.tar.gz -C /app

#Start du serveur front fourni par node
npm run serve
