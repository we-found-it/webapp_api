FROM node:16-alpine

WORKDIR /app

COPY --chown=node ./packaging/scripts/dev/* /.
RUN chmod +x /00-start.sh && mkdir /dependancies && chown node /dependancies && chown node /app

USER node

COPY --chown=node ./*.json ./

RUN npm install --also=dev

# uniquement pour le dev local, pour avoir les deps dispo
RUN tar -czf node_modules.tar.gz node_modules && mv node_modules.tar.gz /dependancies/. && rm -rf node_modules

EXPOSE 4200
HEALTHCHECK --start-period=5s --interval=5s --timeout=2s --retries=3 CMD wget --no-verbose --tries=1 --spider http://localhost:3000 || exit 1

ENTRYPOINT /00-start.sh


