"use strict";
module.exports = () => {
  const Tools = require("../utils/Tools");
  const Requests = require("../utils/Requests");
  const _ = require("lodash");
  return {
    search: async (req, res) => {
      // Considering body.fullname is Firstname Lastname
      const fullname = req.body.fullname.toLowerCase();
      // const usernames = [];
      const username = _.trim(fullname);

      const usernames = Tools.getUsernames(username);
      // const splittedFullname = username.split(/[ -_\.]/);
      // const lastNameWithoutVowels = _.replace(
      //   splittedFullname[1],
      //   /[aeiou]/gi,
      //   ""
      // );

      // _.each([" ", ".", "-", "_", ""], (char) => {
      //   usernames.push(
      //     `${splittedFullname[0]}${
      //       splittedFullname[1] ? `${char}${splittedFullname[1]}` : ""
      //     }`
      //   );
      //   usernames.push(
      //     `${splittedFullname[0]}${
      //       splittedFullname[1] ? `${char}${lastNameWithoutVowels}` : ""
      //     }`
      //   );
      //   if (char !== " ") {
      //     _.each(["_", "-", "."], (specialChar) => {
      //       usernames.push(
      //         `${specialChar}${splittedFullname[0]}${
      //           splittedFullname[1] ? `${char}${splittedFullname[1]}` : ""
      //         }${specialChar}`
      //       );
      //       usernames.push(
      //         `${specialChar}${splittedFullname[0]}${
      //           splittedFullname[1] ? `${char}${lastNameWithoutVowels}` : ""
      //         }${specialChar}`
      //       );
      //       usernames.push(
      //         `${specialChar}${splittedFullname[0]}${
      //           splittedFullname[1] ? `${char}${lastNameWithoutVowels}` : ""
      //         }`
      //       );
      //       usernames.push(
      //         `${splittedFullname[0]}${
      //           splittedFullname[1] ? `${char}${lastNameWithoutVowels}` : ""
      //         }${specialChar}`
      //       );
      //     });
      //   }
      // });
      try {
        const results = await Requests.getUserInfos(
          _.uniq([username, ...usernames])
        );

        return Tools.success(res, results);
      } catch (e) {
        return Tools.internalError(res, req, e);
      }
    },
  };
};
