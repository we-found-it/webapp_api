'use strict';
module.exports = () => {
    const Tools = require('../utils/Tools');
    return {
        tokenCheck: async (req, res, next) => {

            // Check which token is used
            let token = req.headers.authorization;

            if (!token) return Tools.unauthorized(res, {path: 'TokenCheck'}, 'Pas de header Authorization trouvé dans le header');

            const authorization = token.split(' ')[1];

            if (!authorization) return Tools.unauthorized(res, {path: 'TokenCheck'}, 'Pas de token trouvé après le Bearer');

            if (!authorization !== process.env.TOKEN) return Tools.unauthorized(res, {path: 'TokenCheck'}, 'Le token n\'est pas valide');

            next();
        }
    }
}