'use strict';
const Tools = require('../utils/Tools');
const fs = require('fs');
const moment = require('moment');

module.exports = (app) => {

    const TokenMiddleware = require('../middlewares/TokenCheck')();

    app.get('/', function (req, res) {
        return Tools.success(res)
    });
}