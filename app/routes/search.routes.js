'use strict';
const Tools = require('../utils/Tools');

module.exports = (app) => {

    const TokenMiddleware = require('../middlewares/TokenCheck')();
    const SearchController = require('../controllers/SearchController')();

    app.post('/search', SearchController.search);

}