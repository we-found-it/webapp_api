"use strict";
require("dotenv").config();
const axios = require("axios");
const sites = require("../config/sites.json");
const _ = require("lodash");

let Requests = {
  getUserInfos: async (usernames) => {
    const results = [];
    for (const site of sites) {
      await Promise.all(
        _.map(usernames, async (u) => {
          return await axios
            .get(`${site.url.replace("{i_fullname}", u)}`)
            .then((response) => {
              if (response.status === 200) {
                const result = _.omitBy(
                  _.mapValues(site, (value, key) => {
                    return _.get(
                      response.data,
                      value,
                      key === "website" ? value : null
                    );
                  }),
                  _.isNil
                );
                if (_.size(result) > 1) {
                  results.push(result);
                }
              }
            })
            .catch((err) => {
              // console.error(err);
              return null;
            });
        })
      );
    }
    return _.groupBy(results, (x) => x.website);
  },
};
module.exports = Requests;
