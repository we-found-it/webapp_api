"use strict";
require("dotenv").config();
const { v4: uuid } = require("uuid");
const crypto = require("crypto");
const fs = require("fs");
const moment = require("moment");
const _ = require("lodash");

let Tools = {
  uuid: () => {
    return uuid();
  },
  hashPassword: (pwd) => {
    return crypto.createHash("sha256").update(pwd).digest("hex");
  },
  getRequestHeaderToken: (req) => {
    let token = req.headers.authorization;
    if (!token) return false;

    const authorization = token.split(" ")[1];

    if (!authorization) return false;

    return authorization;
  },

  paramMissing: (res, req, message) => {
    Tools.writeLog(message, req);
    return res.status(400).send({ success: false, message: message });
  },

  itemNotFound: (res, req, message = null) => {
    return res
      .status(404)
      .send({ success: false, message: message ? message : "Item not found" });
  },
  internalError: (res, req, err = null) => {
    return res
      .status(500)
      .send({ success: false, message: err ? err : "Internal server error" });
  },
  success: (res, data, message = null) => {
    return res.status(200).send({
      success: true,
      message: message ? message : "Success",
      data: data,
    });
  },
  unauthorized: (res, req, message = null) => {
    return res
      .status(401)
      .send({ success: false, message: message ? message : "Unauthorized" });
  },
  writeLog: (text, req) => {
    const path = `logs/${moment().format("DD-MM-YYYY")}-logs.txt`;
    fs.appendFileSync(
      path,
      `${moment().format("DD-MM-YYYY H:mm:ss")} | '${Tools.getSenderIp(
        req
      )}' "${req.path}" ${text} \n`
    );
  },
  readLogs: (req) => {
    const path = `logs/${moment().format("DD-MM-YYYY")}-logs.txt`;
    try {
      if (fs.existsSync(path)) return fs.readFileSync(path).toString();

      Tools.writeLog("Création du fichier de logs", req);
      Tools.writeLog("Lecture des logs", req);
      return fs.readFileSync(path).toString();
    } catch (err) {
      Tools.writeLog(`Erreur reading logs: ${err}`, req);
      return "Error";
    }
  },
  clearLogs: (req) => {
    fs.unlinkSync(`logs/${moment().format("DD-MM-YYYY")}-logs.txt`);

    return Tools.readLogs(req);
  },
  getSenderIp: (req) => {
    if (!req.headers) return "No ip identified";
    return req.headers["x-forwarded-for"]
      ? req.headers["x-forwarded-for"]
      : "Anonymous sender" || req.connection.remoteAddress
      ? req.connection.remoteAddress
      : "Anonymous sender";
  },
  getUsernames: (username) => {
    return _.map(["-", "_"], (char) => {
      return _.replace(username, " ", char);
    })
  }
};
module.exports = Tools;
