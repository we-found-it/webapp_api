require('dotenv').config();
const express = require('express');
let routes = require('./app/routes');
require('express-group-routes');
const path = require('path');
const http = require('http');
const https = require('https');
const fs = require('fs');
const Tools = require('./app/utils/Tools');
const bodyParser = require('body-parser');
const cors = require('cors');

// const key = fs.readFileSync('./Security/cert.key');
// const cert = fs.readFileSync('./Security/cert.pem');

const app = express();
const port = 3000;

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json());

// const httpsServer = https.createServer({key: key, cert: cert}, app);

console.log('Database initialized');
routes(app);
app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}`)
});

