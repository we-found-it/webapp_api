# webapp_api

## Development
The development mode use docker with mapping file system, hot-reload (with websocket) is available.

### pre-requis
* install docker and docker-compose

### Commands
All commands are run from workdir of project
* Run app
```shell
docker-compose -f packaging/compose/docker-compose-dev.yml -p webapp up -d --build
```
* Show status app
```shell
 docker container ls -a --filter name=webapp
 ```
* Stop app
```shell
docker-compose -f packaging/compose/docker-compose-dev.yml -p webapp down
```
* Show logs
```shell
docker container logs -f webapp_node_1
```
* Run command (example npm install xxx)
```shell
docker container exec --user node -it web_node_1 npm install xxx
```

## Semantic-Release : Automatic Versionning/Changelog/Release Création
- All updates linked to a push on the main branch will automatically update the application version.
- Do a git pull of the main branch to get the correct gitlab.ci-yml file
- Run "npm install"
- When you want to commit
   - run the "npm run cz" command
   - choose the type of commit
   - Then follow the instructions

